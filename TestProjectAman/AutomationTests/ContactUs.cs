using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Remote;
using System;
using TestProjectAman.TestReports;


namespace TestProjectAman
{
    [TestClass]
    public class ContactUs : _IntegrationTest_Base
    {
        public static RemoteWebDriver WebDriver { get; private set; }

        [TestInitialize()]
        public void TestInitialize()
        {
            RootBaseTestInitialise();
            
        }
        [TestCleanup()]
        public void TestCleanup()
        {
           TestCleanUp();
           
        }

        /// <summary>
        /// The purpose of this test is to validate that the user can fill and send the contact us form successfully
        /// </summary>
        [TestMethod]
        public void ContactUs_SignIn_FillAndSubmit__Form()
        {
            try
            {
                ContactUsPageMap contactUs = new ContactUsPageMap(WebDriver);
                contactUs.SendAMessage();
                Reports.exChildTest.Log(Status.Pass, "Passed");
            }

            catch (Exception e)
            {
                Reports.exChildTest.Log(Status.Fail, e.ToString());
                throw;
            }
        }
    }
}
