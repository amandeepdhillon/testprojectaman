using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Remote;
using System;
using TestProjectAman.TestReports;

// this is to run the tests in parallel
//[assembly: Parallelize(Workers = 0, Scope = ExecutionScope.MethodLevel)]

namespace TestProjectAman
{
    [TestClass]
    public class AutomationTestRequested : _IntegrationTest_Base
    {
        public TestContext TestContext { get; set; }
        public static RemoteWebDriver WebDriver { get; private set; }

        [AssemblyInitialize]
        public static void AssemblyInitialise(TestContext testContexts)
        {
           // Reports.ReportLogger("AutomationTestRequested");
            Reports.ReportLogger(testContexts.TestName);
            Reports.exParentTest = Reports.extent.CreateTest(testContexts.TestName).Info("Test started");
            Reports.exChildTest = Reports.exParentTest.CreateNode($"TestName:{ testContexts.TestName}");
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            RootBaseTestInitialise();
          
        }

        [TestCleanup()]
        public void TestCleanup()
        {
           TestCleanUp();
        }

        [AssemblyCleanup]
        public static void AssemblyCleanUp()
        {
            Reports.extent.Flush();
        }

        /// <summary>
        /// The purpose of this test is to validate that the user can shop summer dresses successfully
        /// </summary>
        [TestMethod]
        public void ShopSummerDresses()
        {
            try
            {             
                MainPageMap dresses = new MainPageMap(WebDriver);
                dresses.ShopSummerDresses();
                Reports.exChildTest.Log(Status.Pass, "Passed");
            }
            catch(Exception e)
            {
                Reports.exChildTest.Log(Status.Fail, e.ToString());
                throw;
            }
        }

    }
}
