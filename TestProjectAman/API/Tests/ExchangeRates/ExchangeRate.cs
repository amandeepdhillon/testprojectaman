﻿using System.Collections.Generic;

namespace TestProjectAman.API.Tests.ExchangeRates
{
    public class Currency
    {
        public string currency { get; set; }
        public string amount { get; set; }
        public Dictionary<string, string> rates { get; set; }
    }

    public class Money
    {
        public Currency data { get; set; }
    }
}
