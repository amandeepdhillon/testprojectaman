﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TestProjectAman.Utilities;

namespace TestProjectAman.API.Tests.ExchangeRates
{
    [TestClass]
    public class PostHttpMockTests: BaseTest
    {
        [TestMethod]
        public async Task PostHttpMockTest()
        {
            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(@"[1]"),
            };
            handlerMock.Protected()
                       .Setup<Task<HttpResponseMessage>>(
                             "SendAsync",
                             ItExpr.IsAny<HttpRequestMessage>(),
                             ItExpr.IsAny<CancellationToken>())
                       .ReturnsAsync(response);


            var httpClient = new HttpClient(handlerMock.Object);

            var res = await httpClient.PostAsync("http://test/url", null);

            Assert.IsTrue(res.IsSuccessStatusCode);
        }
    }
}

