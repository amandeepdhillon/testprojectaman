﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using TestProjectAman.API.Tests.ExchangeRates;
using TestProjectAman.TestReports;
using TestProjectAman.Utilities;


namespace TestProjectAman.API.Tests
{
    [TestClass]

    public class ExchangeRatesAPITests : BaseTest
    {
        private HttpClient restClient = new HttpClient();


        [TestInitialize()]
        public void TestInitialize()
        {
            RootBaseTestInitialise();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            TestCleanUp();
        }

        [TestMethod]
        public async Task ExchangeRateGetTest()
        {
            try
            {
                var response = await restClient.GetAsync(new Uri(Url + "exchange-rates"));
                Assert.IsTrue(response.IsSuccessStatusCode);
                Reports.exChildTest.Log(Status.Pass, "Passed ExchangeRateGetTest");
            }
            catch (Exception e)
            {
                Reports.exChildTest.Log(Status.Fail, e.ToString());
                throw;
            }
        }

        [TestMethod]
        public async Task ExchangeRateGetForBtcTest()
        {
            try
            {
                var response = await restClient.GetAsync(new Uri(Url + "exchange-rates?currency=BTC"));
                Assert.IsTrue(response.IsSuccessStatusCode);

                string json = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<Money>(json);

                Assert.AreEqual("BTC", result.data.currency);
                Assert.AreEqual(283, result.data.rates.Keys.Count);
                Reports.exChildTest.Log(Status.Pass, "Passed ExchangeRateGetForBtcTest()");
            }
            catch (Exception e)
            {
                Reports.exChildTest.Log(Status.Fail, e.ToString());
                throw;
            }
        }

        [TestMethod]
        [DataRow("BTC", "USD", "sell")]
        [DataRow("BTC", "USD", "buy")]
        [DataRow("BTC", "USD", "spot")]
        public async Task PricesForPairTest(string currency1, string currency2, string priceType)
        {
            try
            {
                var response = await restClient.GetAsync(new Uri(Url + "prices/" + currency1 + "-" + currency2 + "/" + priceType));
                Assert.IsTrue(response.IsSuccessStatusCode);

                string json = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<Money>(json);

                Assert.AreEqual(currency2, result.data.currency);
                Assert.IsNotNull(result.data.amount);

                Reports.exChildTest.Log(Status.Pass, "Passed PricesForPairTest");
            }
            catch (Exception e)
            {
                Reports.exChildTest.Log(Status.Fail, e.ToString());
                throw;
            }

        }
    }

}

