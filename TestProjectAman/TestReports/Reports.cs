﻿using AventStack.ExtentReports;
using System;
using System.IO;

namespace TestProjectAman.TestReports
{
    public class Reports
    {
        public static ExtentReports extent;
        public static string dirPath;
        public static ExtentTest exParentTest;
        public static ExtentTest exChildTest;

        public static void ReportLogger(string testCaseName)
        {
            extent = new ExtentReports();
            var dir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug\\", "");
            Directory.CreateDirectory(dir + "\\Test_Execution_Reports\\");
           // Random ran = new Random();
           // string randomNo = ran.Next(1000).ToString();
            dirPath = dir + "\\Test_Execution_Reports\\Test_Execution_Reports\\" + "_" + testCaseName;
            AventStack.ExtentReports.Reporter.ExtentHtmlReporter htmlReporter = new AventStack.ExtentReports.Reporter.ExtentHtmlReporter(dirPath);
            htmlReporter.Config.Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;
            extent.AttachReporter(htmlReporter);
            extent.AddSystemInfo("Host Name", System.Net.Dns.GetHostName());

        }
    }
}
