﻿namespace TestProjectAman.Controls.Interfaces
{
    public interface IClickable : IWebObject
    {
        IClickable Click();
    }

    public interface IClickable<out T> : IWebObject<T>
    where T : IClickable
    {
        T Click();
    }
}
