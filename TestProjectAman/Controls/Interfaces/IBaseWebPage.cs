﻿using OpenQA.Selenium;

namespace TestProjectAman.Controls.Interfaces
{
    public interface IBaseWebPage : IHasSpinner, IHasControls
    {
        bool AtRightPlace { get; }

        IBaseWebPage Go();

        string BaseUrl { get; }

        string FullBaseUrl { get; }

        IWebDriver Driver { get; }

        IBaseWebPage WaitLoad(int seconds = 30);

    }

    public interface IBaseWebPage<out T> : IBaseWebPage
        where T : IBaseWebPage
    {
        new T Go();

        new T WaitLoad(int seconds = 30);

        new T WaitForSpinner(int seconds = 60);

        
    }
}