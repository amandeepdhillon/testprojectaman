﻿namespace TestProjectAman.Controls.Interfaces
{
    public interface IHasControls
    {
        void RefreshStaleControls();
    }
}