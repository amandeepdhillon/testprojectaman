﻿namespace TestProjectAman.Controls.Interfaces
{
    public interface IButton<out T> : IClickable<T>
        where T : IClickable
    {
    }
}
