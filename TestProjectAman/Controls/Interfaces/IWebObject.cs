﻿namespace TestProjectAman.Controls.Interfaces
{

    using OpenQA.Selenium;

    public interface IWebObject<out T>
    where T : IWebObject
    {
        
    }

    public interface IWebObject
    {
        IWebObject Parent { get; }

        IBaseWebPage Page { get; }

        By ElementLocation { get; set; }

        IWebElement WebElement { get; }

        IWebObject WaitForDisplayStatus(
            bool displayed,
            int milliseconds = 1000);
    }
}