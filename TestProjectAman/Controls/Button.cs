﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;


namespace TestProjectAman.Controls
{
    using Interfaces;
    using Abstract;
    public class Button : Clickable<Button>, IButton<Button>
    {
        public Button(
           IBaseWebPage page,
           IWebDriver driver,
           string name)
           : this(page, null, driver, name)
        {
        }

        public Button(
            IBaseWebPage page,
            IWebObject parent,
            IWebDriver driver,
            string name)
            : base(page, parent, driver, name)
        {
        }


    }
}
