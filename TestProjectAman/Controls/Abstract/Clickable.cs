﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TestProjectAman.Controls.Interfaces;

namespace TestProjectAman.Controls.Abstract
{
    public abstract class Clickable : BaseWebObject, IClickable
    {
        public delegate void ClickEventHandler(
            IWebObject sender);

        public event ClickEventHandler OnClick;

        protected Clickable(
            IBaseWebPage page,
            IWebObject parent,
            IWebDriver driver,
            string name) : base(page, parent, driver, name)
        {
        }

        public virtual IClickable Click()
        {
            this.Click();
            OnClick?.Invoke(this);
            return this;
        }
    }

    public abstract class Clickable<T> : Clickable
    where T : IClickable
    {
        protected Clickable(
            IBaseWebPage page,
            IWebObject parent,
            IWebDriver driver,
            string name) : base(page, parent, driver, name)
        {
        }

        public new virtual T Click()
        {
            return (T)base.Click();
        }

    }
}
