﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestProjectAman.Controls.Interfaces;

namespace TestProjectAman.Controls.Abstract
{
   public abstract class BaseWebObject : IWebObject
    {
        public delegate void BoundEventHandler(
            IWebObject sender,
            IWebElement webElement);

        protected BaseWebObject(
            IBaseWebPage page,
            IWebObject parent,
            IWebDriver driver,
            string name)
        {
            Page = page;
            Parent = parent;
            Name = name;
            Driver = driver;
           
        }
        public IWebDriver Driver { get; }

        public string Name { get; }
        public IWebObject Parent { get; }

        public IBaseWebPage Page { get; }

        public IWebElement WebElement { get; protected set; }

        public By ElementLocation { get; set; }


        public IWebObject WaitForDisplayStatus(
            bool displayed,
            int milliseconds = 1000)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromMilliseconds(milliseconds))
            {
                PollingInterval = TimeSpan.FromMilliseconds(500)
            };

            try
            {
                wait.Until(
                    _ =>
                    {
                        try
                        {
                            return WebElement.Displayed == displayed;
                        }
                        catch (StaleElementReferenceException)
                        {
                            WebElement = null;
                            return false;
                        }
                    }
                );
            }
            catch (WebDriverTimeoutException ex)
            {
                throw new WebDriverTimeoutException(
                    $"Waited {milliseconds} for {Name} to {(displayed ? "" : "not")} be displayed. Wait timed out.",
                    ex);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    $"Generic error waiting for {Name} to {(displayed ? "" : "not")} be displayed.",
                    ex);
            }

            Thread.Sleep(100);

            return this;
        }

        public event BoundEventHandler OnBind;

        public void UnBindElement()
        {
            WebElement = null;
        }

        protected Button BasicButtonByButtonText(
            string text)
        {
            return new Button(Page, Driver, $"{text} button")
            {
                ElementLocation = By.PartialLinkText(text)
            };
        }

        
        protected T BasicObjectByCssSelector<T>(
            string cssSelector)
            where T : IWebObject
        {
            var control = (T)Activator.CreateInstance(typeof(T), Page, this, Driver, $"{cssSelector} button");
            control.ElementLocation = By.CssSelector(cssSelector);

            return control;
        }

    }
}
