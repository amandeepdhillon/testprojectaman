﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestProjectAman.TestReports;

namespace TestProjectAman.Utilities
{
    public class BaseTest : IDisposable
    {
        protected readonly AppSettingsProvider _appSettingsProvider = new AppSettingsProvider();
        public string Url => _appSettingsProvider.Get("Url");

        public TestContext TestContext { get; set; }
        public void Dispose()
        {

        }
        public void RootBaseTestInitialise()
        {   
            Console.WriteLine(TestContext.TestName);
            Reports.ReportLogger(TestContext.TestName);
            Reports.exParentTest = Reports.extent.CreateTest(TestContext.TestName).Info("Test started");
            Reports.exChildTest = Reports.exParentTest.CreateNode($"TestName:{TestContext.TestName}");
        }
        public void TestCleanUp()
        {
            Reports.extent.Flush();
        }

    }
}
