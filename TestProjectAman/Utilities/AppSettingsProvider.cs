﻿using Microsoft.Extensions.Configuration;

namespace TestProjectAman.Utilities
{
    public class AppSettingsProvider
    {
        private readonly IConfiguration _config;

        public AppSettingsProvider()
        {
            _config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();
        }

        public string Get(string name)
        {
            return _config[name];
        }

    }
}
