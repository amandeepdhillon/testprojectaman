﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using TestProjectAman.Controls;

namespace TestProjectAman
{
    public class MainPageMap : _IntegrationTest_Base
    {
        public MainPageMap(IWebDriver driver)
        {         
        }

        // declare all elements/controls
        IWebElement BtnDresses => driver.FindElement(By.PartialLinkText("DRESSES"));
        IWebElement BtnSummerDresses => driver.FindElement(By.LinkText("SUMMER DRESSES"));
        IWebElement BtnDressesListView => driver.FindElement(By.ClassName("icon-th-list"));
        IWebElement BtnAddToCart=> driver.FindElement(By.PartialLinkText("Add to cart"));
        IWebElement BtnProceedToCheckOut => driver.FindElement(By.CssSelector("a.btn.btn-default.button.button-medium"));
        IWebElement BtnAddQuantity => driver.FindElement(By.ClassName("icon-plus"));
        IWebElement BtnCheckOut => driver.FindElement(By.CssSelector("a.button.btn.btn-default.standard-checkout.button-medium"));
        IWebElement LabelProductName => driver.FindElement(By.CssSelector("span#layer_cart_product_title.product-name"));
        IWebElement TxtEmailAddress => driver.FindElement(By.Id("email"));
        IWebElement TxtPassword => driver.FindElement(By.Id("passwd"));
        IWebElement BtnSubmitLogin => driver.FindElement(By.Id("SubmitLogin"));
        IWebElement BtnProcessAddress => driver.FindElement(By.Name("processAddress"));
        IWebElement ChecBoxTermsAndConditions => driver.FindElement(By.Id("cgv"));
        IWebElement BtnProcessCarrier => driver.FindElement(By.Name("processCarrier"));
        IWebElement BtnBankWire => driver.FindElement(By.ClassName("bankwire"));
        IWebElement BtnConfirmOrder => driver.FindElement(By.CssSelector("button.button.btn.btn-default.button-medium"));
        IWebElement LabelTotalPrice => driver.FindElement(By.Id("total_price"));
        IWebElement LabelOrderComplete => driver.FindElement(By.ClassName("cheque-indent"));

        /// <summary>
        ///Shop by summer dresses
        /// </summary>
        public void ShopSummerDresses()
        {
            BtnDresses.Click();
            WaitMenuOpen();
            BtnSummerDresses.Click();
            WaitForLoading();
            var element = BtnDressesListView;
            Actions actions = new Actions(driver);
            actions.MoveToElement(element);
            actions.Perform();
            BtnDressesListView.Click();
            element = BtnAddToCart;
            actions.MoveToElement(element);
            actions.Perform();
            BtnAddToCart.Click();
            WaitForLoading();
            var productName = LabelProductName.Text;

            new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(ExpectedConditions.ElementToBeClickable(BtnProceedToCheckOut))
                .Click();
            WaitForLoading();
            new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(ExpectedConditions.ElementToBeClickable(BtnAddQuantity))
                .Click();
                 
            BtnCheckOut.Click();          
            Signin(emailAddress, password);          
            BtnProcessAddress.Click();
            WaitForLoading();
            ChecBoxTermsAndConditions.Click();
            BtnProcessCarrier.Click();
            WaitForLoading();
            var totalPrice = LabelTotalPrice.Text;
            BtnBankWire.Click();
            WaitForLoading();
            ScrollTo();
            new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(ExpectedConditions.ElementToBeClickable(BtnConfirmOrder))
                .Click();

            Assert.IsTrue(LabelOrderComplete.Text.Contains("Your order on My Store is complete."));
            Console.WriteLine($"ProductName:{productName}", $"TotalPrice:{totalPrice}" );
        }

        public void Signin(string emailAddress, string password)
        {
            WaitForLoading();
            TxtEmailAddress.SendKeys(emailAddress);
            TxtPassword.SendKeys(password);
            BtnSubmitLogin.Click();
            WaitForLoading();
        }
        
    }
}
