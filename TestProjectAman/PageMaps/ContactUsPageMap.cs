﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace TestProjectAman
{
    public class ContactUsPageMap : _IntegrationTest_Base
    {
       
        public ContactUsPageMap(IWebDriver driver)
        {         
        }
        
        IWebElement BtnContactUs => driver.FindElement(By.Id("contact-link"));
        IWebElement DropdownSubjectHeading => driver.FindElement(By.Id("id_contact"));
        IWebElement TxtMessage => driver.FindElement(By.Id("message"));
        IWebElement TxtEmailAddress => driver.FindElement(By.Id("email"));
        IWebElement TxtOrderReference => driver.FindElement(By.Id("id_order"));
        IWebElement BtnSend => driver.FindElement(By.Id("submitMessage"));
        IWebElement AlertMessage=> driver.FindElement(By.CssSelector("p.alert.alert-success"));

        /// <summary>
        /// Fill contact us form
        /// </summary>
        public void SendAMessage()
        {
            WaitForLoading();
            BtnContactUs.Click();
            WaitForLoading();
            var subject = new SelectElement(DropdownSubjectHeading);
            subject.SelectByText("Customer service");
            TxtEmailAddress.SendKeys(emailAddress);
            TxtOrderReference.SendKeys(RandomNumber(1,1000).ToString());
            TxtMessage.SendKeys(RandomString(25));
            BtnSend.Click();
            WaitForLoading();
            Assert.IsTrue(AlertMessage.Text.Contains("Your message has been successfully sent to our team."));
        }
    }
}
