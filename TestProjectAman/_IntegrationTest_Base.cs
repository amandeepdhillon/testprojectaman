﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Text;
using System.Threading;
using TestProjectAman.TestReports;
using TestProjectAman.Utilities;

namespace TestProjectAman
{
    public class _IntegrationTest_Base 
    {
        protected readonly AppSettingsProvider _appSettingsProvider = new AppSettingsProvider();
        public string Url => _appSettingsProvider.Get("website");

        public const string emailAddress = "amandeep.dhillon@gmail.com";
        public const string password = "Tejas";

        public readonly Random _random = new Random();
   
        public static IWebDriver driver { get; set; } 
        
        public void RootBaseTestInitialise()
        {   
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl(Url);
            driver.Manage().Window.Maximize();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3));
            wait.PollingInterval = TimeSpan.FromMilliseconds(100);
        }

        public void TestCleanUp()
        {
            driver.Close();           
        }

        internal void WaitMenuOpen(string id = "")
        {
            Thread.Sleep(200);
        }

        internal void WaitForLoading(int Seconds = 30)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Seconds));
            wait.PollingInterval = TimeSpan.FromMilliseconds(100);
            Thread.Sleep(200);
        }

        internal void ScrollTo()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollBy(0,250)", "");
        }
      
        // generate a random string
        public string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * _random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        // Generates a random number      
        public int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }
    }
}
